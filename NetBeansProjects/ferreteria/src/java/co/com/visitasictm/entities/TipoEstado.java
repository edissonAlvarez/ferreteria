/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author EDISSON
 */
@Entity
@Table(name = "tipo_estado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoEstado.findAll", query = "SELECT t FROM TipoEstado t")
    , @NamedQuery(name = "TipoEstado.findByIdTipoEstado", query = "SELECT t FROM TipoEstado t WHERE t.idTipoEstado = :idTipoEstado")
    , @NamedQuery(name = "TipoEstado.findByEstadoTipoEstado", query = "SELECT t FROM TipoEstado t WHERE t.estadoTipoEstado = :estadoTipoEstado")})
public class TipoEstado implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoEstado")
    private Collection<Personas> personasCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_tipo_estado")
    private Integer idTipoEstado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "estado_tipo_estado")
    private String estadoTipoEstado;

    public TipoEstado() {
    }

    public TipoEstado(Integer idTipoEstado) {
        this.idTipoEstado = idTipoEstado;
    }

    public TipoEstado(Integer idTipoEstado, String estadoTipoEstado) {
        this.idTipoEstado = idTipoEstado;
        this.estadoTipoEstado = estadoTipoEstado;
    }

    public Integer getIdTipoEstado() {
        return idTipoEstado;
    }

    public void setIdTipoEstado(Integer idTipoEstado) {
        this.idTipoEstado = idTipoEstado;
    }

    public String getEstadoTipoEstado() {
        return estadoTipoEstado;
    }

    public void setEstadoTipoEstado(String estadoTipoEstado) {
        this.estadoTipoEstado = estadoTipoEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoEstado != null ? idTipoEstado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoEstado)) {
            return false;
        }
        TipoEstado other = (TipoEstado) object;
        if ((this.idTipoEstado == null && other.idTipoEstado != null) || (this.idTipoEstado != null && !this.idTipoEstado.equals(other.idTipoEstado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.visitasictm.entities.TipoEstado[ idTipoEstado=" + idTipoEstado + " ]";
    }

    @XmlTransient
    public Collection<Personas> getPersonasCollection() {
        return personasCollection;
    }

    public void setPersonasCollection(Collection<Personas> personasCollection) {
        this.personasCollection = personasCollection;
    }
    
}
