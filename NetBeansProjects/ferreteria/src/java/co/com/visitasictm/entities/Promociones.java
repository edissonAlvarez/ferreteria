/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author EDISSON
 */
@Entity
@Table(name = "promociones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Promociones.findAll", query = "SELECT p FROM Promociones p")
    , @NamedQuery(name = "Promociones.findByIdPromocion", query = "SELECT p FROM Promociones p WHERE p.idPromocion = :idPromocion")
    , @NamedQuery(name = "Promociones.findByValorPromocion", query = "SELECT p FROM Promociones p WHERE p.valorPromocion = :valorPromocion")
    , @NamedQuery(name = "Promociones.findByVigenciaPromocion", query = "SELECT p FROM Promociones p WHERE p.vigenciaPromocion = :vigenciaPromocion")})
public class Promociones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_promocion")
    private Integer idPromocion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor_promocion")
    private int valorPromocion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vigencia_promocion")
    @Temporal(TemporalType.DATE)
    private Date vigenciaPromocion;
    @JoinColumn(name = "id_categoria", referencedColumnName = "id_categoria")
    @ManyToOne(optional = false)
    private Categorias idCategoria;

    public Promociones() {
    }

    public Promociones(Integer idPromocion) {
        this.idPromocion = idPromocion;
    }

    public Promociones(Integer idPromocion, int valorPromocion, Date vigenciaPromocion) {
        this.idPromocion = idPromocion;
        this.valorPromocion = valorPromocion;
        this.vigenciaPromocion = vigenciaPromocion;
    }

    public Integer getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(Integer idPromocion) {
        this.idPromocion = idPromocion;
    }

    public int getValorPromocion() {
        return valorPromocion;
    }

    public void setValorPromocion(int valorPromocion) {
        this.valorPromocion = valorPromocion;
    }

    public Date getVigenciaPromocion() {
        return vigenciaPromocion;
    }

    public void setVigenciaPromocion(Date vigenciaPromocion) {
        this.vigenciaPromocion = vigenciaPromocion;
    }

    public Categorias getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Categorias idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPromocion != null ? idPromocion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Promociones)) {
            return false;
        }
        Promociones other = (Promociones) object;
        if ((this.idPromocion == null && other.idPromocion != null) || (this.idPromocion != null && !this.idPromocion.equals(other.idPromocion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.visitasictm.entities.Promociones[ idPromocion=" + idPromocion + " ]";
    }
    
}
