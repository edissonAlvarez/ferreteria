/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.entities;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author EDISSON
 */
@Entity
@Table(name = "ferreterias")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ferreterias.findAll", query = "SELECT f FROM Ferreterias f")
    , @NamedQuery(name = "Ferreterias.findByIdFerreteria", query = "SELECT f FROM Ferreterias f WHERE f.idFerreteria = :idFerreteria")
    , @NamedQuery(name = "Ferreterias.findByNombreFerreteria", query = "SELECT f FROM Ferreterias f WHERE f.nombreFerreteria = :nombreFerreteria")
    , @NamedQuery(name = "Ferreterias.findByTelefonoFerreteria", query = "SELECT f FROM Ferreterias f WHERE f.telefonoFerreteria = :telefonoFerreteria")
    , @NamedQuery(name = "Ferreterias.findByDireccionFerreteria", query = "SELECT f FROM Ferreterias f WHERE f.direccionFerreteria = :direccionFerreteria")
    , @NamedQuery(name = "Ferreterias.findByPaginaWebFerreteria", query = "SELECT f FROM Ferreterias f WHERE f.paginaWebFerreteria = :paginaWebFerreteria")
    , @NamedQuery(name = "Ferreterias.findByNitFerreteria", query = "SELECT f FROM Ferreterias f WHERE f.nitFerreteria = :nitFerreteria")})
public class Ferreterias implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idFerreteria")
//    private Collection<Inventarios> inventariosCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_ferreteria")
    private Integer idFerreteria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "nombre_ferreteria")
    private String nombreFerreteria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "telefono_ferreteria")
    private String telefonoFerreteria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "direccion_ferreteria")
    private String direccionFerreteria;
    @Size(max = 50)
    @Column(name = "pagina_web_ferreteria")
    private String paginaWebFerreteria;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nit_ferreteria")
    private String nitFerreteria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitud")
    private double latitud;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitud")
    private double longitud;
//    @JoinColumn(name = "id_ubicacion", referencedColumnName = "id_ubicacion")
//    @ManyToOne(optional = false)
//    private Ubicacion idUbicacion;

    public Ferreterias() {
    }

    public Ferreterias(Integer idFerreteria) {
        this.idFerreteria = idFerreteria;
    }

    public Ferreterias(Integer idFerreteria, String nombreFerreteria, String telefonoFerreteria, String direccionFerreteria, String nitFerreteria, double latitud, double longitud) {
        this.idFerreteria = idFerreteria;
        this.nombreFerreteria = nombreFerreteria;
        this.telefonoFerreteria = telefonoFerreteria;
        this.direccionFerreteria = direccionFerreteria;
        this.nitFerreteria = nitFerreteria;
        this.latitud = latitud;
        this.longitud = longitud;
    }

    public Integer getIdFerreteria() {
        return idFerreteria;
    }

    public void setIdFerreteria(Integer idFerreteria) {
        this.idFerreteria = idFerreteria;
    }

    public String getNombreFerreteria() {
        return nombreFerreteria;
    }

    public void setNombreFerreteria(String nombreFerreteria) {
        this.nombreFerreteria = nombreFerreteria;
    }

    public String getTelefonoFerreteria() {
        return telefonoFerreteria;
    }

    public void setTelefonoFerreteria(String telefonoFerreteria) {
        this.telefonoFerreteria = telefonoFerreteria;
    }

    public String getDireccionFerreteria() {
        return direccionFerreteria;
    }

    public void setDireccionFerreteria(String direccionFerreteria) {
        this.direccionFerreteria = direccionFerreteria;
    }

    public String getPaginaWebFerreteria() {
        return paginaWebFerreteria;
    }

    public void setPaginaWebFerreteria(String paginaWebFerreteria) {
        this.paginaWebFerreteria = paginaWebFerreteria;
    }

    public String getNitFerreteria() {
        return nitFerreteria;
    }

    public void setNitFerreteria(String nitFerreteria) {
        this.nitFerreteria = nitFerreteria;
    }
    
    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

//    public Ubicacion getIdUbicacion() {
//        return idUbicacion;
//    }
//
//    public void setIdUbicacion(Ubicacion idUbicacion) {
//        this.idUbicacion = idUbicacion;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idFerreteria != null ? idFerreteria.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ferreterias)) {
            return false;
        }
        Ferreterias other = (Ferreterias) object;
        if ((this.idFerreteria == null && other.idFerreteria != null) || (this.idFerreteria != null && !this.idFerreteria.equals(other.idFerreteria))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.visitasictm.entities.Ferreterias[ idFerreteria=" + idFerreteria + " ]";
    }

//    @XmlTransient
//    public Collection<Inventarios> getInventariosCollection() {
//        return inventariosCollection;
//    }
//
//    public void setInventariosCollection(Collection<Inventarios> inventariosCollection) {
//        this.inventariosCollection = inventariosCollection;
//    }
    
}
