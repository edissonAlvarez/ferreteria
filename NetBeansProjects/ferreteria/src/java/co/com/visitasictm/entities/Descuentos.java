/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author EDISSON
 */
@Entity
@Table(name = "descuentos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Descuentos.findAll", query = "SELECT d FROM Descuentos d")
    , @NamedQuery(name = "Descuentos.findByIdDescuentos", query = "SELECT d FROM Descuentos d WHERE d.idDescuentos = :idDescuentos")
    , @NamedQuery(name = "Descuentos.findByValorDescuento", query = "SELECT d FROM Descuentos d WHERE d.valorDescuento = :valorDescuento")
    , @NamedQuery(name = "Descuentos.findByVigenciaDescuento", query = "SELECT d FROM Descuentos d WHERE d.vigenciaDescuento = :vigenciaDescuento")})
public class Descuentos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_descuentos")
    private Integer idDescuentos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "valor_descuento")
    private int valorDescuento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vigencia_descuento")
    @Temporal(TemporalType.DATE)
    private Date vigenciaDescuento;
    @JoinColumn(name = "id_producto", referencedColumnName = "id_producto")
    @ManyToOne(optional = false)
    private Productos idProducto;

    public Descuentos() {
    }

    public Descuentos(Integer idDescuentos) {
        this.idDescuentos = idDescuentos;
    }

    public Descuentos(Integer idDescuentos, int valorDescuento, Date vigenciaDescuento) {
        this.idDescuentos = idDescuentos;
        this.valorDescuento = valorDescuento;
        this.vigenciaDescuento = vigenciaDescuento;
    }

    public Integer getIdDescuentos() {
        return idDescuentos;
    }

    public void setIdDescuentos(Integer idDescuentos) {
        this.idDescuentos = idDescuentos;
    }

    public int getValorDescuento() {
        return valorDescuento;
    }

    public void setValorDescuento(int valorDescuento) {
        this.valorDescuento = valorDescuento;
    }

    public Date getVigenciaDescuento() {
        return vigenciaDescuento;
    }

    public void setVigenciaDescuento(Date vigenciaDescuento) {
        this.vigenciaDescuento = vigenciaDescuento;
    }

    public Productos getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Productos idProducto) {
        this.idProducto = idProducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDescuentos != null ? idDescuentos.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Descuentos)) {
            return false;
        }
        Descuentos other = (Descuentos) object;
        if ((this.idDescuentos == null && other.idDescuentos != null) || (this.idDescuentos != null && !this.idDescuentos.equals(other.idDescuentos))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.visitasictm.entities.Descuentos[ idDescuentos=" + idDescuentos + " ]";
    }
    
}
