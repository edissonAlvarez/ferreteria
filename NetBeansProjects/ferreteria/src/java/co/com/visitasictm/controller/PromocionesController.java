package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Promociones;
import co.com.visitasictm.facade.PromocionesFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "promocionesController")
@ViewScoped
public class PromocionesController extends AbstractController<Promociones> {

    @Inject
    private CategoriasController idCategoriaController;

    public PromocionesController() {
        // Inform the Abstract parent controller of the concrete Promociones Entity
        super(Promociones.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idCategoriaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Categorias controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdCategoria(ActionEvent event) {
        Promociones selected = this.getSelected();
        if (selected != null && idCategoriaController.getSelected() == null) {
            idCategoriaController.setSelected(selected.getIdCategoria());
        }
    }

}
