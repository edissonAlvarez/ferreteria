package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Categorias;
import co.com.visitasictm.facade.CategoriasFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "categoriasController")
@ViewScoped
public class CategoriasController extends AbstractController<Categorias> {

    public CategoriasController() {
        // Inform the Abstract parent controller of the concrete Categorias Entity
        super(Categorias.class);
    }

}
