package co.com.visitasictm.controller;

import co.com.visitasictm.entities.DetalleFactura;
import co.com.visitasictm.facade.DetalleFacturaFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "detalleFacturaController")
@ViewScoped
public class DetalleFacturaController extends AbstractController<DetalleFactura> {

    public DetalleFacturaController() {
        // Inform the Abstract parent controller of the concrete DetalleFactura Entity
        super(DetalleFactura.class);
    }

}
