package co.com.visitasictm.controller;

import co.com.visitasictm.entities.TipoEstado;
import co.com.visitasictm.facade.TipoEstadoFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "tipoEstadoController")
@ViewScoped
public class TipoEstadoController extends AbstractController<TipoEstado> {

    public TipoEstadoController() {
        // Inform the Abstract parent controller of the concrete TipoEstado Entity
        super(TipoEstado.class);
    }

}
