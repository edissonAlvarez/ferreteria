/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Personas;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author EDISSON
 */
//@SessionScoped
@Named
@ViewScoped
public class sesionController implements Serializable {
    
    public void verificarSesion() {
        try{
            FacesContext context = FacesContext.getCurrentInstance();
            Personas per = (Personas) context.getExternalContext().getSessionMap().get("persona");
            if(per == null || per.getIdTipoUsuario().getDescripcionTipoUsuario().equals("User")) {
                System.out.println("permiso denegado");
                context.getExternalContext().redirect("permisos.xhtml");
            }
        } catch(Exception e) {
            //log para guardar registros
            
        }
    }    
    
    public void verificarUsuario() {
        try{
            FacesContext context = FacesContext.getCurrentInstance();
            Personas per = (Personas) context.getExternalContext().getSessionMap().get("persona");
            if(per == null || per.getIdTipoUsuario().getDescripcionTipoUsuario().equals("Admin")) {
                System.out.println("permiso denegado");
                context.getExternalContext().redirect("permisos.xhtml");
            }
        } catch(Exception e) {
            //log para guardar registros
            
        }
    }    
    
}
