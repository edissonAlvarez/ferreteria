package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Personas;
import co.com.visitasictm.facade.PersonasFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "personasController")
@ViewScoped
public class PersonasController extends AbstractController<Personas> {

    @Inject
    private TipoEstadoController idTipoEstadoController;
    @Inject
    private TipoUsuariosController idTipoUsuarioController;

    public PersonasController() {
        // Inform the Abstract parent controller of the concrete Personas Entity
        super(Personas.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idTipoEstadoController.setSelected(null);
        idTipoUsuarioController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the TipoEstado controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdTipoEstado(ActionEvent event) {
        Personas selected = this.getSelected();
        if (selected != null && idTipoEstadoController.getSelected() == null) {
            idTipoEstadoController.setSelected(selected.getIdTipoEstado());
        }
    }

    /**
     * Sets the "selected" attribute of the TipoUsuarios controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdTipoUsuario(ActionEvent event) {
        Personas selected = this.getSelected();
        if (selected != null && idTipoUsuarioController.getSelected() == null) {
            idTipoUsuarioController.setSelected(selected.getIdTipoUsuario());
        }
    }

}
