package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Ferreterias;
import co.com.visitasictm.facade.FerreteriasFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "ferreteriasController")
@ViewScoped
public class FerreteriasController extends AbstractController<Ferreterias> {

    @Inject
    private UbicacionController idUbicacionController;
    private InventariosController idFerreteriaController;

    public FerreteriasController() {
        // Inform the Abstract parent controller of the concrete Ferreterias Entity
        super(Ferreterias.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idUbicacionController.setSelected2(null);
    }

    /**
     * Sets the "selected" attribute of the Ubicacion controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
//    public void prepareIdUbicacion(ActionEvent event) {
//        Ferreterias selected = this.getSelected2();
//        if (selected != null && idUbicacionController.getSelected2() == null) {
//            idUbicacionController.setSelected2(selected.getIdUbicacion());
//        }
//    }
//    
//    
//    public void prepareIdInventarios(ActionEvent event) {
//        Ferreterias selected = this.getSelected2();
//        if (selected != null && idUbicacionController.getSelected2() == null) {
//            idUbicacionController.setSelected(selected.getIdUbicacion());
//        }
//    }

}
