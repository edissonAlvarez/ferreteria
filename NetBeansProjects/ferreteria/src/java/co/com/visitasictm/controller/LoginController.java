/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Personas;
import co.com.visitasictm.facade.LoginFacadeLocal;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author EDISSON
 */
@Named
@ViewScoped
public class LoginController implements Serializable {

    @EJB
    private LoginFacadeLocal EJBLogin;
    private Personas persona;

    @PostConstruct
    public void init() {
        persona = new Personas();
    }

    public Personas getPersona() {
        return persona;
    }

    public void setPersona(Personas persona) {
        this.persona = persona;
    }

    public String iniciarSesion() {
        Personas per;
        String redireccion = null;
        try {
            per = EJBLogin.iniciarSesion(persona);
            if (per != null) {

                if (per.getIdTipoEstado().getEstadoTipoEstado().equals("Inactivo")) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Su usuario esta inactivo"));
                } else {
                    if (per.getIdTipoUsuario().getDescripcionTipoUsuario().equals("Admin")) {

                        if (per != null) {
                            //Almacenar en la sesión de JSF
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("persona", per);
                            redireccion = "/index?faces-redirect=true";
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales incorrectas."));

                        }
                    } else {
                        if (per != null) {
                            //Almacenar en la sesión de JSF
                            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("persona", per);
                            redireccion = "/indexUser?faces-redirect=true";
                        } else {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales incorrectas."));

                        }
                    }

                }
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso", "Credenciales incorrectas."));
            }

        } catch (Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", e.toString()));

        }
        return redireccion;
    }
}
