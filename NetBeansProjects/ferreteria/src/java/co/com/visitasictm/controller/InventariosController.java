package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Inventarios;
import co.com.visitasictm.facade.InventariosFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "inventariosController")
@ViewScoped
public class InventariosController extends AbstractController<Inventarios> {

    @Inject
    private FerreteriasController idFerreteriaController;
    @Inject
    private ProductosController idProductoController;

    public InventariosController() {
        // Inform the Abstract parent controller of the concrete Inventarios Entity
        super(Inventarios.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idFerreteriaController.setSelected(null);
        idProductoController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Ferreterias controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdFerreteria(ActionEvent event) {
        Inventarios selected = this.getSelected();
        if (selected != null && idFerreteriaController.getSelected() == null) {
            idFerreteriaController.setSelected2(selected.getIdFerreteria());
        }
    }

    /**
     * Sets the "selected" attribute of the Productos controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdProducto(ActionEvent event) {
        Inventarios selected = this.getSelected();
        if (selected != null && idProductoController.getSelected() == null) {
            idProductoController.setSelected(selected.getIdProducto());
        }
    }

}
