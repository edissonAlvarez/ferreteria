package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Productos;
import co.com.visitasictm.facade.ProductosFacade;
import co.com.visitasictm.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "productosController")
@ViewScoped
public class ProductosController extends AbstractController<Productos> {

    @Inject
    private CategoriasController idCategoriaController;
    @Inject
    private MobilePageController mobilePageController;

    public ProductosController() {
        // Inform the Abstract parent controller of the concrete Productos Entity
        super(Productos.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idCategoriaController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Categorias controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdCategoria(ActionEvent event) {
        Productos selected = this.getSelected();
        if (selected != null && idCategoriaController.getSelected() == null) {
            idCategoriaController.setSelected(selected.getIdCategoria());
        }
    }

}
