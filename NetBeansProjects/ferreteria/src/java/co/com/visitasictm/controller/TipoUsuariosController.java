package co.com.visitasictm.controller;

import co.com.visitasictm.entities.TipoUsuarios;
import co.com.visitasictm.facade.TipoUsuariosFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "tipoUsuariosController")
@ViewScoped
public class TipoUsuariosController extends AbstractController<TipoUsuarios> {

    public TipoUsuariosController() {
        // Inform the Abstract parent controller of the concrete TipoUsuarios Entity
        super(TipoUsuarios.class);
    }

}
