package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Ubicacion;
import co.com.visitasictm.facade.UbicacionFacade;
import co.com.visitasictm.controller.util.MobilePageController;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;

@Named(value = "ubicacionController")
@ViewScoped
public class UbicacionController extends AbstractController<Ubicacion> {

    @Inject
    private MobilePageController mobilePageController;

    public UbicacionController() {
        // Inform the Abstract parent controller of the concrete Ubicacion Entity
        super(Ubicacion.class);
    }

}
