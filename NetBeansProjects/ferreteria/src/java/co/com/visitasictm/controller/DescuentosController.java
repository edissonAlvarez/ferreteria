package co.com.visitasictm.controller;

import co.com.visitasictm.entities.Descuentos;
import co.com.visitasictm.facade.DescuentosFacade;
import javax.inject.Named;
import javax.faces.view.ViewScoped;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;

@Named(value = "descuentosController")
@ViewScoped
public class DescuentosController extends AbstractController<Descuentos> {

    @Inject
    private ProductosController idProductoController;

    public DescuentosController() {
        // Inform the Abstract parent controller of the concrete Descuentos Entity
        super(Descuentos.class);
    }

    /**
     * Resets the "selected" attribute of any parent Entity controllers.
     */
    public void resetParents() {
        idProductoController.setSelected(null);
    }

    /**
     * Sets the "selected" attribute of the Productos controller in order to
     * display its data in its View dialog.
     *
     * @param event Event object for the widget that triggered an action
     */
    public void prepareIdProducto(ActionEvent event) {
        Descuentos selected = this.getSelected();
        if (selected != null && idProductoController.getSelected() == null) {
            idProductoController.setSelected(selected.getIdProducto());
        }
    }

}
