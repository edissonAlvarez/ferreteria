/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.service;

import co.com.visitasictm.entities.Ubicacion;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author EDISSON
 */
@Stateless
@Path("ubicacion")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class UbicacionFacadeREST extends AbstractFacade<Ubicacion> {

    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;

    public UbicacionFacadeREST() {
        super(Ubicacion.class);
    }

    @POST
    @Override
//    @Consumes({"application/xml", "application/json"})
    public void create(Ubicacion entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
//    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Ubicacion entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
//    @Produces({"application/xml", "application/json"})
    public Ubicacion find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
//    @Produces({"application/xml", "application/json"})
    public List<Ubicacion> findAll() {
//        return super.findAll();
//        String consulta = "SELECT u FROM Ubicacion u";
//        String consulta = "SELECT * FROM ubicacion";
//        String consulta = "SELECT nombre_ferreteria FROM ferreterias INNER JOIN ubicacion ON ferreterias.id_ubicacion = ubicacion.id_ubicacion";
//        String consulta = "SELECT c, p.latitud FROM Ferreterias c JOIN c.idUbicacion p";
        String consulta = "SELECT f FROM Ferreterias f";
        Query query = em.createQuery(consulta);
        List<Ubicacion> lista = query.getResultList();
//        return super.findAll();
        System.out.println(lista);
        return lista;
    }

    @GET
    @Path("{from}/{to}")
//    @Produces({"application/xml", "application/json"})
    public List<Ubicacion> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

}
