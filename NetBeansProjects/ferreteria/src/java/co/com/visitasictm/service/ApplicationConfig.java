/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author Leo Montes
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(co.com.visitasictm.service.CategoriasFacadeREST.class);
        resources.add(co.com.visitasictm.service.DescuentosFacadeREST.class);
        resources.add(co.com.visitasictm.service.FerreteriasFacadeREST.class);
        resources.add(co.com.visitasictm.service.InventariosFacadeREST.class);
        resources.add(co.com.visitasictm.service.ProductosFacadeREST.class);
        resources.add(co.com.visitasictm.service.UbicacionFacadeREST.class);
    }
    
}
