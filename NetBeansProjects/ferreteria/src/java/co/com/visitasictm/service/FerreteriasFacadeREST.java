/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.service;

import co.com.visitasictm.entities.Ferreterias;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Edisson
 */
@Stateless
@Path("ferreterias")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FerreteriasFacadeREST extends AbstractFacade<Ferreterias> {

    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;

    public FerreteriasFacadeREST() {
        super(Ferreterias.class);
    }

    @POST
    @Override
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Ferreterias entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
//    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, Ferreterias entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Ferreterias find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Ferreterias> findAll() {
        String consulta = "SELECT f FROM Ferreterias f";
//        String consulta = "SELECT c, p.latitud FROM Ferreterias c JOIN c.idUbicacion p";
//        String consulta = "SELECT c, p.nombreFerreteria FROM Inventarios c JOIN c.idFerreteria p";
//        String consulta = "SELECT f.nombreFerreteria FROM Ferreterias f";

        Query query = em.createQuery(consulta);
        List<Ferreterias> lista = query.getResultList();
//        return super.findAll();
        System.out.println(lista);
        return lista;
//        return super.findAll();
    }
    
    @GET
    @Path("/count")
    public List<Ferreterias> countFerreterias() {
        String consulta = "SELECT COUNT(d.idFerreteria) FROM Ferreterias d";
        Query query = em.createQuery(consulta);
        List<Ferreterias> lista = query.getResultList();
        return lista;
    }

    @GET
    @Path("{from}/{to}")
//    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Ferreterias> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
