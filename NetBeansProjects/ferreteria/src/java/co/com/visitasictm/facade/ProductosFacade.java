/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.facade;

import co.com.visitasictm.entities.Productos;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import co.com.visitasictm.entities.Productos_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.com.visitasictm.entities.Categorias;

/**
 *
 * @author EDISSON
 */
@Stateless
public class ProductosFacade extends AbstractFacade<Productos> {

    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ProductosFacade() {
        super(Productos.class);
    }

    public boolean isIdCategoriaEmpty(Productos entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Productos> productos = cq.from(Productos.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(productos, entity), cb.isNotNull(productos.get(Productos_.idCategoria)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Categorias findIdCategoria(Productos entity) {
        return this.getMergedEntity(entity).getIdCategoria();
    }
    
}
