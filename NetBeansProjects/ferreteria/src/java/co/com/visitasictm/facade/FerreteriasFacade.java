/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.facade;

import co.com.visitasictm.entities.Ferreterias;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import co.com.visitasictm.entities.Ferreterias_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.com.visitasictm.entities.Ubicacion;

/**
 *s
 * @author EDISSON
 */
@Stateless
public class FerreteriasFacade extends AbstractFacade<Ferreterias> {

    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public FerreteriasFacade() {
        super(Ferreterias.class);
    }

//    public boolean isIdUbicacionEmpty(Ferreterias entity) {
//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
//        Root<Ferreterias> ferreterias = cq.from(Ferreterias.class);
//        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(ferreterias, entity), cb.isNotNull(ferreterias.get(Ferreterias_.idUbicacion)));
//        return em.createQuery(cq).getResultList().isEmpty();
//    }
//
//    public Ubicacion findIdUbicacion(Ferreterias entity) {
//        return this.getMergedEntity(entity).getIdUbicacion();
//    }
    
}
