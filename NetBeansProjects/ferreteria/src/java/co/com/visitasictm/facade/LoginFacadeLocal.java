/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.facade;

import co.com.visitasictm.entities.Personas;
import javax.ejb.Local;

/**
 *
 * @author EDISSON
 */
@Local
public interface LoginFacadeLocal {
    
    Personas iniciarSesion(Personas per);
    
}
