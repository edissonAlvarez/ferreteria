/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.facade;

import co.com.visitasictm.entities.Personas;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import co.com.visitasictm.entities.Personas_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.com.visitasictm.entities.TipoEstado;
import co.com.visitasictm.entities.TipoUsuarios;

/**
 *
 * @author EDISSON
 */
@Stateless
public class PersonasFacade extends AbstractFacade<Personas> {

    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PersonasFacade() {
        super(Personas.class);
    }

    public boolean isIdTipoEstadoEmpty(Personas entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Personas> personas = cq.from(Personas.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(personas, entity), cb.isNotNull(personas.get(Personas_.idTipoEstado)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public TipoEstado findIdTipoEstado(Personas entity) {
        return this.getMergedEntity(entity).getIdTipoEstado();
    }

    public boolean isIdTipoUsuarioEmpty(Personas entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Personas> personas = cq.from(Personas.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(personas, entity), cb.isNotNull(personas.get(Personas_.idTipoUsuario)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public TipoUsuarios findIdTipoUsuario(Personas entity) {
        return this.getMergedEntity(entity).getIdTipoUsuario();
    }
    
}
