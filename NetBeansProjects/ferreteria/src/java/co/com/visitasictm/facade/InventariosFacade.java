/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.facade;

import co.com.visitasictm.entities.Inventarios;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import co.com.visitasictm.entities.Inventarios_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.com.visitasictm.entities.Ferreterias;
import co.com.visitasictm.entities.Productos;

/**
 *
 * @author EDISSONs
 */
@Stateless
public class InventariosFacade extends AbstractFacade<Inventarios> {

    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public InventariosFacade() {
        super(Inventarios.class);
    }

    public boolean isIdFerreteriaEmpty(Inventarios entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inventarios> inventarios = cq.from(Inventarios.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inventarios, entity), cb.isNotNull(inventarios.get(Inventarios_.idFerreteria)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Ferreterias findIdFerreteria(Inventarios entity) {
        return this.getMergedEntity(entity).getIdFerreteria();
    }

    public boolean isIdProductoEmpty(Inventarios entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Inventarios> inventarios = cq.from(Inventarios.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(inventarios, entity), cb.isNotNull(inventarios.get(Inventarios_.idProducto)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Productos findIdProducto(Inventarios entity) {
        return this.getMergedEntity(entity).getIdProducto();
    }
    
}
