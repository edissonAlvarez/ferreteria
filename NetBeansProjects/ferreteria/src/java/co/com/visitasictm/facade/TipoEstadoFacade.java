/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.facade;

import co.com.visitasictm.entities.TipoEstado;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author EDISSON
 */
@Stateless
public class TipoEstadoFacade extends AbstractFacade<TipoEstado> {

    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public TipoEstadoFacade() {
        super(TipoEstado.class);
    }
    
}
