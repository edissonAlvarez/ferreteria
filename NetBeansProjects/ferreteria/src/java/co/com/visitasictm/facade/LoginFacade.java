/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.facade;

import co.com.visitasictm.entities.Personas;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author EDISSON
 */
@Stateless
public class LoginFacade extends AbstractFacade<Personas> implements LoginFacadeLocal{
    
    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public LoginFacade() {
        super(Personas.class);
    }
    
    @Override
    public Personas iniciarSesion(Personas per) {
        Personas persona = null;
        String consulta;
        try{
            consulta = "FROM Personas u WHERE u.usuarioPersona = ?1 and u.clavePersona = ?2";
            Query query = em.createQuery(consulta);
            query.setParameter(1, per.getUsuarioPersona());
            query.setParameter(2, per.getClavePersona());
            List<Personas> lista = query.getResultList();
            
            if(!lista.isEmpty()) {
                persona = lista.get(0);
            }
            
        } catch(Exception e) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Error al iniciar sesión."));
        } finally {
//            em.close();
        }
        return persona;
        
    }
    
}
