/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.facade;

import co.com.visitasictm.entities.Descuentos;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import co.com.visitasictm.entities.Descuentos_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.com.visitasictm.entities.Productos;

/**
 *
 * @author EDISSON
 *
 */
@Stateless
public class DescuentosFacade extends AbstractFacade<Descuentos> {

    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DescuentosFacade() {
        super(Descuentos.class);
    }

    public boolean isIdProductoEmpty(Descuentos entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Descuentos> descuentos = cq.from(Descuentos.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(descuentos, entity), cb.isNotNull(descuentos.get(Descuentos_.idProducto)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Productos findIdProducto(Descuentos entity) {
        return this.getMergedEntity(entity).getIdProducto();
    }
    
}
