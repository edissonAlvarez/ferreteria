/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.visitasictm.facade;

import co.com.visitasictm.entities.Promociones;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import co.com.visitasictm.entities.Promociones_;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import co.com.visitasictm.entities.Categorias;

/**
 *
 * @author EDISSON
 */
@Stateless
public class PromocionesFacade extends AbstractFacade<Promociones> {

    @PersistenceContext(unitName = "WebJEEPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public PromocionesFacade() {
        super(Promociones.class);
    }

    public boolean isIdCategoriaEmpty(Promociones entity) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Promociones> promociones = cq.from(Promociones.class);
        cq.select(cb.literal(1L)).distinct(true).where(cb.equal(promociones, entity), cb.isNotNull(promociones.get(Promociones_.idCategoria)));
        return em.createQuery(cq).getResultList().isEmpty();
    }

    public Categorias findIdCategoria(Promociones entity) {
        return this.getMergedEntity(entity).getIdCategoria();
    }
    
}
