/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var mymap;
function initialize() {
    
    mymap = L.map('mapid').setView([5.06811, -75.51732], 13);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 22,
        minZoom: 10,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox/streets-v11'
    }).addTo(mymap);
    ubicacion();
    graficosRequest();
    graficosProductos();
}

function ubicacion() {
    try {
        $.ajax({
            url: "http://localhost:8080/ferreteria/webresources/ferreterias",
            type: 'GET',
            dataType: 'json',
            context: document.body,
            data: {
               // "id": 1
            },
            success: function (datos) {
                mostrar(datos);
            }
        });
    } catch (e) {
        alert(e.message);
    }
    
}

function graficosRequest() {
    try {
        $.ajax({
            url: "http://localhost:8080/ferreteria/webresources/ferreterias/count",
            type: 'GET',
            dataType: 'text',
            context: document.body,
            data: {
                // "id": 1
            },
            success: function (datos) {
                grafico(datos);
            }
        });
    } catch (e) {
        alert(e.message);
    }
}

function graficosProductos() {
    try {
        $.ajax({
            url: "http://localhost:8080/ferreteria/webresources/productos/count",
            type: 'GET',
            dataType: 'text',
            context: document.body,
            data: {
                // "id": 1
            },
            success: function (datos) {
                graficoProductos(datos);
            }
        });
    } catch (e) {
        alert(e.message);
    }
}

function mostrar(datos) {
    var ubicacion = datos;
    for (var i = 0; i < ubicacion.length; i++) {
        var latitud = ubicacion[i].latitud;
        var longitud = ubicacion[i].longitud;
//        var id = ubicacion[i].idUbicacion.idUbicacion;
        var nombreFerreteria = ubicacion[i].nombreFerreteria;
        var paginaWebFerreteria = ubicacion[i].paginaWebFerreteria;
        var telefonoFerreteria = ubicacion[i].telefonoFerreteria;
        var direccionFerreteria = ubicacion[i].direccionFerreteria;
        addMarker(latitud, longitud, nombreFerreteria, paginaWebFerreteria, telefonoFerreteria, direccionFerreteria);
    }
}
function addMarker(latitud, longitud,  nombreFerreteria, paginaWebFerreteria, telefonoFerreteria, direccionFerreteria) {
    L.marker([latitud, longitud]).addTo(mymap)
//            .bindPopup("<b>Ferreteria " + nombreFerreteria + "</b><br />Ubicación" + " Lat: " + latitud + " Long: " + longitud + " N°" + content);
            .bindPopup("<b>Ferreteria:</b> " + nombreFerreteria + "<br />" +
            "<b>Direccion:</b> " +  direccionFerreteria + "<br />" + 
            "<b>Telefono:</b> " + telefonoFerreteria + "<br />" +
            "<b>Pagina web:</b> " + paginaWebFerreteria);

}




function grafico(cantidad) {
    
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Ferreterias'],
            datasets: [{
                label: 'Cantidad de ferreterias',
                data: [cantidad],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}

function graficoProductos(cantidad) {
    
    var ctx = document.getElementById('myChart2').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Productos'],
            datasets: [{
                label: 'Cantidad de productos',
                data: [cantidad],
                backgroundColor: [
                    'rgba(75, 192, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(75, 192, 192, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}