function graficosRequest() {
    try {
        $.ajax({
            url: "http://localhost:8080/ferreteria/webresources/ferreterias/count",
            type: 'GET',
            dataType: 'text',
            context: document.body,
            data: {
               // "id": 1
            },
            success: function (datos) {
                grafico(datos);
            }
        });
    } catch (e) {
        alert(e.message);
    }
}

function grafico(cantidad) {
    
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Ferreterias'],
            datasets: [{
                label: '# of Votes',
                data: [3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}